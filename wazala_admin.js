// $Id$
// file: wazala_admin.js
// eye catching web 2.0 stuff for the Wazala admin page


// an after-the-DOM is loaded wrapper:
// $(function() {  <- this is the traditional jquery after-the-DOM style,
// while below is the preferred Drupal method:
Drupal.behaviors.initWazalaAdmin = function( context ) {

  'use strict';
  
  
  // this examines the Wazala widget Type (embed or overlay) and makes the
  // overlay fieldset collapsed or open based upon overlay being selected:
  var wazalaWidgetTypeHandler = function() {
    
        // is this an overlay or embed?
        var nature = $('select#edit-wazala-type').val();
  
        // is the overlay fieldset collapsed? 
        var state = $('div#wazala-admin-overlay-fieldset fieldset').hasClass('collapsed');
  
        // open or close the field set based upon our overlay or embed nature:
        if (nature === 'overlay') {
           if (state === true) {
              $('div#wazala-admin-overlay-fieldset fieldset').removeClass('collapsed');
              wazalaOverlayLabelHandler();
           }
        }
        else if (nature === 'embed') {
           if (state === false) {
              $('div#wazala-admin-overlay-fieldset fieldset').addClass('collapsed');
           }
        }
  }

  
  // attach a callback to the Type widget to show/hide when Type changes:
  $('select#edit-wazala-type').change(wazalaWidgetTypeHandler);
  
  
  // this examines the overlay label and makes the custom widget label
  // visible based upon custom being selected:
  var wazalaOverlayLabelHandler = function() {
    
        // is custom selected?
        var labelType = $('select#edit-wazala-label').val();
        // is the customLabel widget visible?
        var visibility = $('div#edit-wazala-customLabel-wrapper').css('display');
        
        if (labelType === 'Custom') {
           if (visibility === 'none') {
                $('div#edit-wazala-customLabel-wrapper').fadeIn('fast');
           }
        }
        else if (visibility !== 'none') {
           $('div#edit-wazala-customLabel-wrapper').fadeOut('fast');
        }
  }

  
  // attach a callback to the overlay custom label widget to show/hide when label changes:
  $('select#edit-wazala-label').change(wazalaOverlayLabelHandler);
  
  // run logic once upon page load:
  wazalaWidgetTypeHandler();
  wazalaOverlayLabelHandler();
};

