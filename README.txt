; $Id$
File: README.txt

This file accompanies the wazala module for Drupal 6, and contains a description of this module.

This module was developed and is maintained by Blake Senftner, drupal.org/user/162580 

Wazala is an external-to-Drupal eCommerce solution for Drupal. 

Wazala offers a low impact, easy method for web site owners and developers to include eCommerce in their Drupal
sites, without the additional, often significant, effort other Drupal eCommerce solutions require. 

This document covers:
---------------------
* Contents of this package
* Wazala Features - not the Wazala module's features
* Wazala Module Features - Drupal integration specifics
* Module Installation and Use
* Where to get help and learn more

Contents of this package:
-------------------------
* README.txt       - this document
* wazala_admin.css - CSS for the module's administration page
* wazala_admin.js  - javascript for the module's administration page
* wazala.info      - Drupal 6 format .info file for this module
* wazala.module    - Drupal 6 Wazala integration module

Wazala Features - not the Wazala module's features:
---------------------------------------------------
* Places a "store" button on your site which launches an overlay web application hosting your online store.
* Optionally, the "store" may be embedded above your site header, or (soon) within a Drupal block.
* Sell physical goods, or digital downloads.
* Products may have multiple images, options, and price points. 
* Payment gateways such as Paypal, Google Checkout and Authorize.net are supported.
* The overlay "store" web application sports multiple options & styles for product layout and store appearance. 
* The overlay "store" web application dims your site, similar to Lightbox, while a user shops.
* Integrated inventory management.
* A rich coupon and discount management interface.
* A store management Dashboard at Wazala.com
* Multilingual, currently supporting 15 languages.
* Because your Wazala store is external to your Drupal site, the store can appear anywhere you can post the Wazala
  javascript that launches your store. If you have multiple Drupal, WP, or other sites, they can all host the same
  Wazala store. 

Wazala Module Features - Drupal integration specifics
-----------------------------------------------------
* This module handles all the Wazala logic integration necessary to get your store up and operating in Drupal 6.
* Note that the "store owner" will still need to register at http://www.wazala.com/pricing/ in order to get their
  Wazala account and merchant account created and linked so the store can process transactions.
* Currently, the module supports two store "modes" - overlay and embedded. 
  * Overlay places a tab with your store's name at the top of your site's pages. User clicks launch the store as
    an overlay on top of your site, like Lightbox.
  * Embeded places the store at the top of your site, pushing the site's header down. In the future, this mode's
    functionality is going to be changed so the store appears in a Drupal block, enabling you to control the
    store's appearance and placement as you would with any Drupal block. 

Module Installation and Use
---------------------------
1) Place the wazala directory inside your sites/all/modules directory, just like any other Drupal contrib module.
2) Navigate to admin/build/modules/list, scroll down to the Wazala module, enable it, and save the configuration.
3) Navigate to admin/settings/wazala to view the Administration form for this module.
4) The key information to transfer from your Wazala registration at wazala.com is your Store's Nickname, once that
   is saved within your configuration, your store is linked to Wazala and active. If you have not yet registered 
   at wazala.com, you can use "wazala" as your temporary store nickname. 


Where to get help and learn more
--------------------------------
* For the Wazala Quick Start Documentation, see https://www.wazala.com/start/
* for the Wazala Support, FAQ, and forums, see http://support.wazala.com/home
* For the Wazala Online Store API Documentation, see https://www.wazala.com/api/docs/
